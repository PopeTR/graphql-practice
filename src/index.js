const { PrismaClient } = require("@prisma/client");
const { ApolloServer } = require("apollo-server");
const fs = require("fs");
const path = require("path");

// 1
// IN schema.graphql you will find the schema. It defines the Query types. This field has the type String!. The exclamation mark in the type definition means that this field is required and can never be null.

// Above we have three root fields: users and user on Query as well as createUser on Mutation. The additional definition of the User type is required because otherwise the schema definition would be incomplete.

// This is our dummy data (replaced with Prisma data later in the tutorial)
// let links = [
//   {
//     id: "link-0",
//     url: "www.howtographql.com",
//     description: "Fullstack tutorial for GraphQL",
//   },
//   {
//     id: "link-1",
//     url: "www.twitter.com",
//     description: "Social media tweet platform",
//   },
// ];
// A new integer variable that simply serves as a very rudimentary way to generate unique IDs for newly created Link elements. (replaced with prisma data later in the tutorial)
// let idCount = links.length;

// The resolvers object is the actual implementation of the GraphQL schema. Notice how its structure is identical to the structure of the type definition inside typeDefs: Query.info.
const resolvers = {
  Query: {
    info: () => `This is the API of a Hackernews Clone`,
    feed: async (parent, args, context) => {
      return context.prisma.link.findMany();
    },
    link: (parent, args, context) => {
      const findLink = context.prisma.link.findIndex(args.id);
      return findLink;
    },
  },
  //   The implementation of the post resolver first creates a new link object, then adds it to the existing links list and finally returns the new link.
  Mutation: {
    post: (parent, args, context, info) => {
      const newLink = context.prisma.link.create({
        data: {
          url: args.url,
          description: args.description,
        },
      });

      return newLink;
    },
    updateLink: async (parent, args, context, info) => {
      const update = await context.prisma.link.update({
        where: {
          id: parseInt(args.id),
        },
        data: {
          id: parseInt(args.id),
          url: args.url,
          description: args.description,
        },
      });
      return update;
      //   let updatedLink;
      //   const links = context.prisma.link.findMany()
      //   links.forEach((innerlink) => {
      //     if (innerlink.id === args.id) {
      //       updatedLink = { ...innerlink, ...args };
      //       return updatedLink;
      //     }
      //     return innerlink;
      //   });
      //   return updatedLink;
    },
    deleteLink: (parent, args, context, info) => {
      return context.prisma.link.delete({
        where: { id: parseInt(args.id) },
      });
      // Code for deleting a link when not using a database
      //   const links = context.prisma.link.findMany();
      //   const removeIndex = links.findIndex((item) => item.id === args.id);
      //   const removedLink = links[removeIndex];
      //   links.splice(removeIndex, 1);
      //   return removedLink;
    },
    //   This is not needed as GQL infers what Link looks like from the Query feed above.
    //   Link: {
    //     id: (parent) => parent.id,
    //     description: (parent) => parent.description,
    //     url: (parent) => parent.url,
  },
};
/** Why do we use parent? On the first level, it invokes the feed resolver and returns the entire data stored in links.
 * For the second execution level, the GraphQL server is smart enough to invoke the resolvers of the Link type (because thanks to the schema,
 * it knows that feed returns a list of Link elements) for each element inside the list that was returned on the previous resolver level.
 * Therefore, in all of the three Link resolvers, the incoming parent object is the element inside the links list. */

const prisma = new PrismaClient();
// 3
// Finally, the schema and resolvers are bundled and passed to ApolloServer which is imported from apollo-server. This tells the server what API operations are accepted and how they should be resolved.
const server = new ApolloServer({
  // the below is how we link the schema file
  typeDefs: fs.readFileSync(path.join(__dirname, "schema.graphql"), "utf8"),
  resolvers,
  //   This is the context object that you add to connect your database with GQL. You’ll now be able to access context.prisma in all of your resolvers.

  context: {
    prisma,
  },
});

server.listen().then(({ url }) => console.log(`Server is running on ${url}`));
