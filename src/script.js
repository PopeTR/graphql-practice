// 1 the imported prisma client node module from the generation in CLI
const { PrismaClient } = require("@prisma/client");

// 2 instantiate prisma
const prisma = new PrismaClient();

//3 Define an async function called main to send queries to the database. You will write all your queries inside this function.
async function main() {
  const newLink = await prisma.link.create({
    data: {
      description: "Full stack tutorial for GraphQL",
      url: "www.howtographql.com",
    },
  });
  const allLinks = await prisma.link.findMany();
  console.log(allLinks);
}

//4 call the main function
main()
  .catch((e) => {
    throw e;
  })
  // 5 Close the database connections when the script terminates.
  .finally(async () => {
    await prisma.$disconnect();
  });
